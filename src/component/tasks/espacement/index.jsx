import React, { useState, useEffect } from 'react'
import Button from '@mui/material/Button'

const Espacement = (props) => {
    // VARIABLES
    const isTextRemainVisible = true
    const [showConsigne, setShowConsigne] = useState(true)

    // sessions
    const [textInitial, setTextInitial] = useState(() => {
        // initial text
        let initText = props.useTextPerso ? props.textPerso : props.text
        // Split the text into an array of words
        let words = initText.split(' ')
        const spaced = words.map((word) => {
            // Generate a random number of spaces
            const numSpaces = props.randomSpace? Math.floor(Math.random() * props.space):props.space
            // Add the spaces before the word
            return '&nbsp;'.repeat(numSpaces) + word
        })
        // Join the array of spaced-out words back into a string
        console.log(spaced.join(' '))
        return spaced.join(' ')
    })
    const [stimText, setStimText] = useState(textInitial)
    const stimTextR = textInitial
    const [nIteration, setnIteration] = useState(0)

    useEffect(() => {
        
        const keyDownHandler = (event) => {
            if (event.key === ' ' || event.key === '') {
                event.preventDefault()
                setShowConsigne(false)
            }
        }
        document.addEventListener('keydown', keyDownHandler)

        //const interval = setInterval(() => setStimText(stimText.substring(1,stimText.length)), 1000);
        return () => {
            document.removeEventListener('keydown', keyDownHandler)
        }
    }, [])

    const emulateSpaceEvent = () => {
        let e = new KeyboardEvent('keydown', { keyCode: 32, which: 32 })
        document.dispatchEvent(e)
    }

    return (
        <div className="taskwin">
            <h3
                style={
                    showConsigne
                        ? {
                              color: 'black',
                          }
                        : {
                              color: 'white',
                          }
                }
            >
                Appuyez sur espace ou cliquez sur le texte lorsque vous êtes prêt à commencer à lire
            </h3>
            {nIteration < textInitial.length ? (
                <p dangerouslySetInnerHTML={{ __html: stimText }} 
                    style={{ fontSize: props.size + 'em', lineHeight: props.size + 'em'}}
                    onClick={() => emulateSpaceEvent()}
                />
                  
            ) : (
                <h3>FINI !</h3>
            )}
            <br />
            <br />
            <Button
                variant="contained"
                color="secondary"
                onClick={() => {
                    props.back()
                }}
            >
                Retour
            </Button>
        </div>
    )
}

export default Espacement
