import React, { useState, useEffect, Fragment } from 'react'
import Button from '@mui/material/Button'
import arrayShuffle from 'array-shuffle'

const Discrimination = (props) => {
    // Listes mots
    const [nTrial, setNTrial] = useState(0)
    const [list, setList] = useState(props.listeMots)
    const [listWords, setListWords] = useState([])
    const [showWord, setShowWord] = useState(true)
    const [selectedTarget, setSelectedTarget] = useState([])
    const [showConsigne, setShowConsigne] = useState(true)
    const [score, setScore] = useState(NaN)
    const [totalScore, setTotalScore] = useState(0)
    const [showBackButton, setShowBackButton] = useState(true)

    const [timeoutId, setTimeoutId] = useState(null)

    useEffect(() => {
        console.log(list)

        setShowBackButton(true)
        if (selectedTarget.length > props.nTarget - 1) {
            trialResult()
        }
    }, [selectedTarget])
    const emulateSpaceEvent = () => {
        let e = new KeyboardEvent('keydown', { keyCode: 32, which: 32 })
        document.dispatchEvent(e)
    }
    // Handle click
    const clickEvent = (index) => {
        if (!selectedTarget.includes(index)) {
            setSelectedTarget((selectedTarget) => [...selectedTarget, index])
        }
    }

    // Session logics
    const createTrial = (word) => {
        console.log('createTrial')
        setScore(NaN)
        setSelectedTarget([])
        setShowWord(true)
        let targets = new Array(props.nTarget).fill(word)
        for (let i = 0; i < props.nDist; i++) {
            targets.push(createDistractor(word, targets))
        }
        setListWords(arrayShuffle(targets))
        // Time limit
        let timeout = setTimeout(() => {
            setShowWord(false)
        }, props.speed * 1000)
        setTimeoutId(timeout)
    }
    const createDistractor = (strings, targets) => {
        let iteration = 0
        if (strings.length < 4) {
            window.alert('Distrateur non valide (< 4 caractères)')
            return 'NON VALIDE'
        } else {
            while (true) {
                iteration = iteration + 1
                let substrings = strings.substring(1, strings.length - 1) // remove 1st and last letter
                let shuffleStr = arrayShuffle(substrings.split('')).join('') // transform substrings to array, shuffle, then join
                let distractor = strings[0] + shuffleStr + strings[strings.length - 1] // concatenate 1st and last letter with the  shuffle string between
                console.log(targets.includes(distractor))
                if (!targets.includes(distractor)) {
                    return distractor
                } else if (iteration > 1000) {
                    //window.alert("Erreur : limite d'itération atteinte ")
                    let substringsDist = distractor.substring(1, distractor.length - 1) // remove 1st and last letter of the distractor
                    let randomLetters = []
                    for (let i = 0; i < substringsDist.length; i++) {
                        // Generate a random number between 97 and 122 (corresponding to the ASCII codes
                        // for the lowercase letters 'a' to 'z')
                        let randomCode = Math.floor(Math.random() * (122 - 97 + 1)) + 97
                        // Convert the ASCII code to a letter and store it in a variable
                        randomLetters.push(String.fromCharCode(randomCode))
                    }
                    let distractor2 =
                        distractor[0] + randomLetters.join('') + distractor[distractor.length - 1] // concatenate 1st and last letter with the  shuffle string between
                    return distractor2
                }
            }
        }
    }

    const trialResult = () => {
        console.log(nTrial)
        clearTimeout(timeoutId)
        let arraySelectedWord = []
        selectedTarget.map((i) => {
            arraySelectedWord.push(listWords[i])
        })

        let score = arraySelectedWord.every((val) => val === arraySelectedWord[0]) ? 1 : 0 // test if all value are equal
        setTotalScore(totalScore + score)
        setScore(score)
        setShowBackButton(false)
        console.log('nTrial')
        console.log(nTrial)
        setTimeout(() => {
            setNTrial(nTrial + 1)
            if (nTrial < list.length) {
                createTrial(list[nTrial])
            } else {
                // session END
                setShowBackButton(true)
            }
        }, 1500)
    }
    return (
        <div>
            {showConsigne && (
                <Fragment>
                    <h3>Cliquez sur les mots qui sont identiques le plus rapidement possible </h3>
                    <Button
                        variant="contained"
                        onClick={() => {
                            setShowConsigne(false)
                            createTrial(list[nTrial])
                            setShowConsigne(false)
                            setNTrial(nTrial + 1)
                        }}
                    >
                        Demarrer
                    </Button>
                </Fragment>
            )}

            {nTrial - 1 < list.length ? (
                isNaN(score) ? (
                    <div style={{ fontSize: props.size + 'em', lineHeight: props.size + 'em' }}>
                        {listWords.map((word, index) => (
                            <p
                                key={'w' + index}
                                className={`${
                                    selectedTarget.includes(index)
                                        ? 'discrimTargetSelected'
                                        : 'discrimTarget'
                                }`}
                                onClick={(e) => clickEvent(index, word)}
                            >
                                {showWord ? word : 'xxxx'}
                            </p>
                        ))}
                    </div>
                ) : (
                    <h3>{score === 1 ? 'Correct' : 'Incorrect'}</h3>
                )
            ) : (
                <Fragment>
                    <h3>FINI !</h3>
                    <p>
                        {' '}
                        Le pourcentage de réponses correctes est de{' '}
                        {parseInt((totalScore / list.length) * 100)}%
                    </p>
                </Fragment>
            )}
            <br />
            <br />
            {showBackButton && (
                <Button
                    variant="contained"
                    color="secondary"
                    onClick={() => {
                        props.back()
                    }}
                >
                    Retour
                </Button>
            )}
        </div>
    )
}

export default Discrimination
