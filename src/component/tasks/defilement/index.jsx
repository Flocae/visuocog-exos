import React, { useState, useEffect } from 'react'
import Button from '@mui/material/Button'
import Marquee from 'react-fast-marquee'

// https://www.react-fast-marquee.com/documentation

const Defilement = (props) => {
    // VARIABLES
    const isTextRemainVisible = props.textRemainVisible
    const [showConsigne, setShowConsigne] = useState(true)
    // sessions
    const textInitial = props.useTextPerso ? props.textPerso : props.text

    const [stimText, setStimText] = useState(textInitial)
    const stimTextR = textInitial
    const [nIteration, setnIteration] = useState(0)
    // scroll parames
    const [play, setPlay] = useState(false)

    useEffect(() => {
      
        const keyDownHandler = (event) => {
            console.log(event)
            if (event.key === ' ' || event.key === '') {
                event.preventDefault()
                setShowConsigne(false)
                setPlay(true)
            }
        }
        document.addEventListener('keydown', keyDownHandler)


        return () => {
            document.removeEventListener('keydown', keyDownHandler)
        }
    }, [])
    const emulateSpaceEvent = () => {
        let e = new KeyboardEvent('keydown', { keyCode: 32, which: 32 })
        document.dispatchEvent(e)
    }
    return (
        <div className="taskwin">
            <h3 
                style={
                    showConsigne
                        ? {
                              color: 'black',
                          }
                        : {
                              color: 'white',
                          }
                }
            >
                Appuyez sur espace ou cliquez sur le texte lorsque vous êtes prêt pour lancer la
                lecture{' '}
            </h3>
            {nIteration < textInitial.length ? (
                <div style={{width:"50%", marginLeft:"auto",marginRight:"auto",fontSize:props.size + 'em'}} onClick={()=>emulateSpaceEvent()}>
                    <Marquee play={play} speed={props.speed} loop={1} gradient={false} style={{height:props.size + 'em'}}>
                        {stimText}
                    </Marquee>
                </div>
            ) : (
                <h3>FINI !</h3>
            )}
            <br />
            <br />
            <Button
                variant="contained"
                color="secondary"
                onClick={() => {
                    props.back()
                }}
            >
                Retour
            </Button>
        </div>
    )
}

export default Defilement
