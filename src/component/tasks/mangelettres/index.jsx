import React, { useState, useEffect } from 'react'
import Button from '@mui/material/Button'

const Mangelettres = (props) => {
    // VARIABLES
    const isTextRemainVisible = props.textRemainVisible
    const [showConsigne, setShowConsigne] = useState(true)
    // sessions
    const textInitial = props.useTextPerso
        ? props.textPerso
        : props.text

    const [stimText, setStimText] = useState(textInitial)
    const stimTextR = textInitial
    const [nIteration, setnIteration] = useState(0)

    useEffect(() => {
        let currentSpeed = 50
        let interval
        if (props.speed > 0) {
            currentSpeed = currentSpeed / props.speed
        } else if (props.speed < 0) {
            currentSpeed = currentSpeed * -props.speed
        }

        const keyDownHandler = (event) => {
            console.log(event)
            if (event.key === ' ' || event.key === "") {
                event.preventDefault()
                if (interval === undefined) {
                    setShowConsigne(false)
                    interval = setInterval(() => {
                        setStimText((stimText) => stimText.substring(1, stimText.length))
                        setnIteration((nIteration) => nIteration + 1)
                    }, currentSpeed)
                }
            }
        }
        document.addEventListener('keydown', keyDownHandler)

        //const interval = setInterval(() => setStimText(stimText.substring(1,stimText.length)), 1000);
        return () => {
            clearInterval(interval)
            document.removeEventListener('keydown', keyDownHandler)
        }
    }, [])

    const emulateSpaceEvent = () => {
        let e = new KeyboardEvent('keydown', { keyCode: 32, which: 32 });
        document.dispatchEvent(e);
    }

    return (
        <div className="taskwin">
            <h3
                style={
                    showConsigne
                        ? {
                              color: 'black',
                          }
                        : {
                              color: 'white',
                          }
                }
            >
                Appuyez sur espace ou cliquez sur le texte lorsque vous êtes prêt pour lancer la lecture{' '}
            </h3>
            {nIteration < textInitial.length ? (
                <p
                    style={{ fontSize: props.size + 'em', lineHeight: props.size + 'em' }}
                    onClick={()=>emulateSpaceEvent()}
                >
                    <span
                        style={
                            isTextRemainVisible
                                ? {
                                      color: 'gray',
                                  }
                                : {
                                      color: 'white',
                                  }
                        }
                    >
                        {stimTextR.slice(0, nIteration)}
                    </span>
                    {stimText}
                </p>
            ) : (
                <h3>FINI !</h3>
            )}
            <br />
            <br />
            <Button
                variant="contained"
                color="secondary"
                onClick={() => {
                    props.back()
                }}
            >
                Retour
            </Button>
        </div>
    )
}

export default Mangelettres
