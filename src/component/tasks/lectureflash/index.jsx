import React, { useState, useEffect } from 'react'
import arrayShuffle from 'array-shuffle'
import Button from '@mui/material/Button'
import { wordlist } from './wordlist'

const LectureFlash = (props) => {
    // Listes mots

    const [wordList, setWordList] = useState(() => {
        let list =
            props.nLetters === 0
                ? wordlist[props.type]// si nLetters = 0, récupère toute la liste
                : wordlist[props.type].filter((item) => {
                      return item.length === props.nLetters
                  })
        list = arrayShuffle(list)
        list = list.slice(0, props.lengthList) // longueur max de la liste
        let arrayWord = []
        list.map((w) => {
            arrayWord.push('...')
            arrayWord.push(w)
        })
        arrayWord.push('...')
        return arrayWord
    })
    const [showConsigne, setShowConsigne] = useState(true)

    // sessions
    const [nIteration, setnIteration] = useState(0)

    useEffect(() => {
        let interval
        const keyDownHandler = (event) => {
            if (event.key === ' ' || event.key === '') {
                event.preventDefault()
                if (interval === undefined) {
                    setShowConsigne(false)
                    interval = setInterval(() => {
                        setnIteration((nIteration) => nIteration + 1)
                    }, props.speed)
                }
            }
        }
        document.addEventListener('keydown', keyDownHandler)

        return () => {
            clearInterval(interval)
            document.removeEventListener('keydown', keyDownHandler)
        }
    }, [])

    const emulateSpaceEvent = () => {
        let e = new KeyboardEvent('keydown', { keyCode: 32, which: 32 })
        document.dispatchEvent(e)
    }

    return (
        <div className="taskwin">
            <h3
                onClick={() => emulateSpaceEvent()}
                style={
                    showConsigne
                        ? {
                              color: 'black',
                          }
                        : {
                              color: 'white',
                          }
                }
            >
                Appuyez sur la barre d'espace ou cliquez sur cette consigne pour lancer la liste des
                mots
            </h3>
            {nIteration < wordList.length ? (
                <p style={{ fontSize: props.size + 'em', lineHeight: props.size + 'em' }}>
                    {wordList[nIteration]}
                </p>
            ) : (
                <h3>FINI !</h3>
            )}
            <br />
            <br />
            <Button
                variant="contained"
                color="secondary"
                onClick={() => {
                    props.back()
                }}
            >
                Retour
            </Button>
        </div>
    )
}
export default LectureFlash
