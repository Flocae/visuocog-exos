import React from 'react'

const Footer = () => {
    const currentVersion = '0.9'
    return (
        <footer>
            <div className="footer-container">
                <a href="https:///florentcaetta.fr" target="_blank">
                    <img
                        src="https://florentcaetta.fr/wp-content/uploads/2016/12/logoFlo.gif"
                        height={30}
                    />
                </a>
                <p>
                    <a href="https:///florentcaetta.fr" target="_blank" rel="noreferrer">
                        Florent Caetta
                    </a>
                    , Laura Antonini
                </p>
                <p>v{currentVersion}
                <a href='https://gitlab.com/Flocae/visuocog-exos/-/blob/main/CHANGELOG.md'target="_blank"> Changelog</a></p>
            </div>
        </footer>
    )
}

export default Footer
