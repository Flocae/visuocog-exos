import React, { Fragment, useState } from 'react'
import Espacement from '../../tasks/espacement'
import Button from '@mui/material/Button'
import Slider from '@mui/material/Slider'
import Typography from '@mui/material/Typography'
import Box from '@mui/material/Box'
import FormGroup from '@mui/material/FormGroup'
import FormControlLabel from '@mui/material/FormControlLabel'
import Checkbox from '@mui/material/Checkbox'
import TextField from '@mui/material/TextField'
import Select from '@mui/material/Select'
import InputLabel from '@mui/material/InputLabel'
import MenuItem from '@mui/material/MenuItem'
import FormControl from '@mui/material/FormControl'
import { text } from '../mangelettres/text'

const EspacementMenu = (props) => {
    // VARIABLES
    // UI
    const [showMenu, setShowMenu] = useState(true)
    // Session
    const [space, setSpace] = useState(1)
    const [size, setSize] = useState(1)
    const [randomSpace, setRandomSpace] = useState(false)
    const [textSelect, setTextSelect] = useState('loup')
    const [useTextPerso, setUseTextPerso] = useState(false)
    const [textPerso, setTextPerso] = useState('')

    const handleChange = (event) => {
        setTextSelect(event.target.value)
    }

    const back = () => {
        setShowMenu(true)
    }
    function valuetext(value) {
        return `${value}°C`
    }
    return (
        <div>
            {showMenu ? (
                <Fragment>
                    <h2>Espacement</h2>
                    <br />
                    <Box sx={{ width: 250 }} style={{ textAlign: 'left', margin: 'auto' }}>
                        <Typography id="input-slider" gutterBottom>
                            Espacement {randomSpace&&"maximal"} : {space}
                        </Typography>
                        <Slider
                            aria-label="Space"
                            value={space}
                            getAriaValueText={valuetext}
                            valueLabelDisplay="auto"
                            step={1}
                            marks
                            min={1}
                            max={40}
                            onChange={(e) => setSpace(e.target.value)}
                        />
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={randomSpace}
                                    onChange={(e) => setRandomSpace(e.target.checked)}
                                />
                            }
                            label="Aléatoire"
                        />


                        <Typography id="input-slider" gutterBottom>
                            Taille de la police : {size} em
                        </Typography>
                        <Slider
                            aria-label="Size"
                            value={size}
                            getAriaValueText={valuetext}
                            valueLabelDisplay="auto"
                            step={0.5}
                            marks
                            min={1}
                            max={3}
                            onChange={(e) => setSize(e.target.value)}
                        />
                        <FormGroup>
                            <FormControl fullWidth>
                                <InputLabel id="textSelect">Texte</InputLabel>
                                <Select
                                    id="demo-simple-select"
                                    value={textSelect}
                                    label="Texte"
                                    onChange={handleChange}
                                    disabled={useTextPerso}
                                >
                                    <MenuItem value={'loup'}>Le loup</MenuItem>

                                    <MenuItem value={'lune'}>
                                        {' '}
                                        Comment s'est formée la lune ?
                                    </MenuItem>
                                    <MenuItem value={''}> Comment s'est formée la lune ?</MenuItem>
                                    <MenuItem value={'gateauChoco'}>
                                        {' '}
                                        Recette d'un gâteau au chocolat
                                    </MenuItem>

                                    <MenuItem value={'chatsouris_primaire'}>
                                        {' '}
                                        Chat et souris niveau primaire{' '}
                                    </MenuItem>
                                    <MenuItem value={'chatsouris_bac'}>
                                        {' '}
                                        Chat et souris niveau bac{' '}
                                    </MenuItem>
                                </Select>
                            </FormControl>

                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={useTextPerso}
                                        onChange={(e) => setUseTextPerso(e.target.checked)}
                                    />
                                }
                                label="Texte personnalisé"
                            />
                            {useTextPerso && (
                                <TextField
                                    id="filled-multiline-static"
                                    multiline
                                    value={textPerso}
                                    rows={4}
                                    variant="filled"
                                    onChange={(e) => setTextPerso(e.target.value)}
                                />
                            )}
                        </FormGroup>
                    </Box>

                    <br />

                    <Button
                        variant="contained"
                        onClick={() => {
                            setShowMenu(false)
                        }}
                    >
                        Demarrer
                    </Button>
                    <br />
                    <br />
                    <Button
                        color="secondary"
                        size="small"
                        variant="contained"
                        onClick={() => {
                            props.back()
                        }}
                    >
                        Retour
                    </Button>

                    <br />
                </Fragment>
            ) : (
                <Espacement
                    space={space}
                    size={size}
                    randomSpace={randomSpace}
                    useTextPerso={useTextPerso}
                    text={text[textSelect]}
                    textPerso={textPerso}
                    back={back}
                />
            )}
        </div>
    )
}

export default EspacementMenu
