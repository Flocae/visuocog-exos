import React, { Fragment, useState } from 'react'
import Button from '@mui/material/Button'
import Slider from '@mui/material/Slider'
import Typography from '@mui/material/Typography'
import Box from '@mui/material/Box'
import Radio from '@mui/material/Radio'
import RadioGroup from '@mui/material/RadioGroup'
import FormControlLabel from '@mui/material/FormControlLabel'
import Discrimination from '../../tasks/discrimination'
import { motsFrequents } from '../../../assets/data/motsFrequents'
import arrayShuffle from 'array-shuffle'

const DiscriminationMenu = (props) => {
    // VARIABLES
    // UI
    const [showMenu, setShowMenu] = useState(true)
    // Session
    const [wordLength, setWordLength] = useState(5)
    const [speed, setSpeed] = useState(5)
    const [nTarget, setnTarget] = useState(2)
    const [nDist, setnDist] = useState(2)
    const [size, setSize] = useState(1)
    const [type, setType] = React.useState('mots')
    const [lengthList, setLengthList] = React.useState(20)
    const [listeMots, setListeMots] = React.useState([])
    const back = () => {
        setShowMenu(true)
    }

    return (
        <div>
            {showMenu ? (
                <Fragment>
                    <h2>Discrimination</h2>
                    <br />
                    <Box sx={{ width: 250 }} style={{ textAlign: 'left' }}>
                        <Typography id="input-slider" gutterBottom>
                            Longueur des mots : {wordLength} lettres
                        </Typography>
                        <Slider
                            aria-label="wordLength"
                            value={wordLength}
                            valueLabelDisplay="auto"
                            step={1}
                            marks
                            min={5}
                            max={10}
                            onChange={(e) => setWordLength(e.target.value)}
                        />
                        <Typography id="input-slider" gutterBottom>
                            Temps de présentation : {speed} sec
                        </Typography>
                        <Slider
                            aria-label="Vitesse"
                            value={speed}
                            valueLabelDisplay="auto"
                            step={1}
                            marks
                            min={1}
                            max={10}
                            onChange={(e) => setSpeed(e.target.value)}
                        />
                        <Typography id="input-slider" gutterBottom>
                            Nombe de cibles : {nTarget}
                        </Typography>
                        <Slider
                            aria-label="Vitesse"
                            value={nTarget}
                            valueLabelDisplay="auto"
                            step={1}
                            marks
                            min={2}
                            max={4}
                            onChange={(e) => setnTarget(e.target.value)}
                        />
                        <Typography id="input-slider" gutterBottom>
                            Nombe de distracteurs : {nDist}
                        </Typography>
                        <Slider
                            aria-label="Distracteurs"
                            value={nDist}
                            valueLabelDisplay="auto"
                            step={1}
                            marks
                            min={1}
                            max={4}
                            onChange={(e) => setnDist(e.target.value)}
                        />
                        <Typography id="input-slider" gutterBottom>
                            Taille de la police : {size} em
                        </Typography>
                        <Slider
                            aria-label="Size"
                            value={size}
                            valueLabelDisplay="auto"
                            step={0.5}
                            marks
                            min={1}
                            max={3}
                            onChange={(e) => setSize(e.target.value)}
                        />
                        <Typography id="input-slider" gutterBottom>
                            Longueur de la liste : {lengthList} mots
                        </Typography>
                        <Slider
                            aria-label="Size"
                            value={lengthList}
                            valueLabelDisplay="auto"
                            step={5}
                            marks
                            min={5}
                            max={50}
                            onChange={(e) => setLengthList(e.target.value)}
                        />
                        <Typography>Type</Typography>
                        <RadioGroup
                        row
                            aria-labelledby="radio-buttons-group-label"
                            value={type}
                            onChange={(e) => {
                                setType(e.target.value)
                            }}
                            name="radio-buttons-group"
                        >
                            <FormControlLabel
                                value="mots"
                                control={<Radio />}
                                label="Mots"
                                disabled={true}
                            />
                            <FormControlLabel
                                value="nonmots"
                                control={<Radio />}
                                label="Non-Mots"
                                disabled={true}
                            />
                        </RadioGroup>
                    </Box>

                    <br />

                    <Button
                        variant="contained"
                        onClick={() => {
                            let listeMotsSelect
                            listeMotsSelect = motsFrequents.filter((word) => {
                                return word.label.length === wordLength
                            })

                            //
                            let listeMotsSelectFinal = []
                            listeMotsSelect.map((word) => listeMotsSelectFinal.push(word['label']))
                            //console.log(listeMotsSelectFinal)
                            listeMotsSelectFinal=arrayShuffle(listeMotsSelectFinal)
                            // Start Session
                            if (listeMotsSelectFinal.length > lengthList) {
                                listeMotsSelectFinal.splice(
                                    lengthList,
                                    listeMotsSelectFinal.length - lengthList
                                )
                            }
                            setListeMots(listeMotsSelectFinal)
                            setShowMenu(false)
                        }}
                    >
                        Demarrer
                    </Button>
                    <br />
                    <br />
                    <Button
                        color="secondary"
                        size="small"
                        variant="contained"
                        onClick={() => {
                            props.back()
                        }}
                    >
                        Retour
                    </Button>

                    <br />
                </Fragment>
            ) : (
                <Discrimination
                    speed={speed}
                    nTarget={nTarget}
                    nDist={nDist}
                    size={size}
                    type={type}
                    lengthList={lengthList}
                    back={back}
                    listeMots={listeMots}
                />
            )}
        </div>
    )
}
export default DiscriminationMenu
