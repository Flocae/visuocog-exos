import React, { Fragment, useState } from 'react'
import LectureFlash from '../../tasks/lectureflash'
import Button from '@mui/material/Button'
import Slider from '@mui/material/Slider'
import Typography from '@mui/material/Typography'
import Box from '@mui/material/Box'
import Radio from '@mui/material/Radio'
import RadioGroup from '@mui/material/RadioGroup'
import FormControlLabel from '@mui/material/FormControlLabel'

const LectureFlashMenu = (props) => {
    // VARIABLES
    // UI
    const [showMenu, setShowMenu] = useState(true)
    // Session
    const [speed, setSpeed] = useState(400)
    const [nLetters, setNLetters] = React.useState(0)
    const [size, setSize] = useState(1)
    const [type, setType] = React.useState('usuels')
    const [lengthList, setLengthList] = React.useState(20)

    const marks = [
        {
            value: 0,
        },
        {
            value: 1,
        },
        {
            value: 2,
        },
        {
            value: 3,
        },
        {
            value: 4,
        },
        {
            value: 5,
        },
        {
            value: 6,
        },
        {
            value: 7,
        },
        {
            value: 8,
        },
        {
            value: 9,
        },
        {
            value: 10,
        },
    ]
    const back = () => {
        setShowMenu(true)
    }

    return (
        <div>
            {showMenu ? (
                <Fragment>
                    <h2>Lecture flash</h2>
                    <br />
                    <Box sx={{ width: 250 }} style={{ textAlign: 'left' }}>
                        <Typography id="input-slider" gutterBottom>
                            Temps de présentation : {speed} ms
                        </Typography>
                        <Slider
                            aria-label="Vitesse"
                            value={speed}
                            valueLabelDisplay="auto"
                            step={100}
                            marks
                            min={100}
                            max={5000}
                            onChange={(e) => setSpeed(e.target.value)}
                        />

                        <Typography id="input-slider-nLetter" gutterBottom>
                            Nombre de lettres : {nLetters === 0 ? 'aléatoire 4-10' : nLetters}
                        </Typography>
                        <Slider
                            aria-label="Size"
                            value={nLetters}
                            valueLabelDisplay="auto"
                            step={null}
                            marks={marks}
                            onChange={(e) => setNLetters(e.target.value)}
                            max={10}
                        />

                        <Typography id="input-slider" gutterBottom>
                            Taille de la police : {size} em
                        </Typography>
                        <Slider
                            aria-label="Size"
                            value={size}
                            valueLabelDisplay="auto"
                            step={0.5}
                            marks
                            min={1}
                            max={3}
                            onChange={(e) => setSize(e.target.value)}
                        />

                        <Typography id="input-slider" gutterBottom>
                            Longueur max. de la liste : {lengthList} mots
                        </Typography>
                        <Slider
                            aria-label="Size"
                            value={lengthList}
                            valueLabelDisplay="auto"
                            step={5}
                            marks
                            min={5}
                            max={40}
                            onChange={(e) => setLengthList(e.target.value)}
                        />
                         <Typography>Type</Typography>
                                <RadioGroup
                                    aria-labelledby="radio-buttons-group-label"
                                    value={type}
                                    onChange={(e) => {
                                        setType(e.target.value)
                                    }}
                                    name="radio-buttons-group"
                                    
                                >
                                    <FormControlLabel
                                        value="usuels"
                                        control={<Radio />}
                                        label="Mots usuels"
                                        disabled={nLetters===1 || nLetters === 2 || nLetters === 3}
                                    />
                                    <FormControlLabel
                                        value="irreguliers"
                                        control={<Radio />}
                                        label="Mots irréguliers"
                                        disabled={nLetters===1 || nLetters === 2 || nLetters === 3}
                                    />
                                </RadioGroup>
                    </Box>

                    <br />

                    <Button
                        variant="contained"
                        onClick={() => {
                            setShowMenu(false)
                        }}
                        
                    >
                        Demarrer
                    </Button>
                    <br />
                    <br />
                    <Button
                        color="secondary"
                        size="small"
                        variant="contained"
                        onClick={() => {
                            props.back()
                        }}
                    >
                        Retour
                    </Button>

                    <br />
                </Fragment>
            ) : (
                <LectureFlash
                    speed={speed}
                    nLetters={nLetters}
                    size={size}
                    type={nLetters===1 || nLetters===2 || nLetters===3?"autres":type}
                    lengthList={lengthList}
                    back={back}
                />
            )}
        </div>
    )
}
export default LectureFlashMenu
