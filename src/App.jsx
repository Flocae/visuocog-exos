import './App.css'
import { useState, Fragment } from 'react'
import MangeLettresMenu from './component/menus/mangelettres'
import LectureFlashMenu from './component/menus/lectureflash'
import DiscriminationMenu from './component/menus/discrimination'
import DefilementMenu from './component/menus/defilement'
import EspacementMenu from './component/menus/espacement'
import Button from '@mui/material/Button'
import Footer from './component/footer'

import axios from 'axios'

function App() {
    const [currentExercice, setCurrentExercice] = useState('')

    // AXIOS OPENAI
    const [prompt, setPrompt] = useState('Raconte moi une histoire avec un loup et une souris')
    const [response, setResponse] = useState('')
    const showTestButton=false;
    const handleSubmit = () => {
        //e.preventDefault()

        // Send a request to the server with the prompt
        
        axios
            .post('http://localhost:8080/chat', { prompt })
            .then((res) => {
                // Update the response state with the server's response
                setResponse(res.data)
                console.log(res.data)
            })
            .catch((err) => {
                console.error(err)
            })
    }

    const back = () => {
        setCurrentExercice('')
    }

    return (
        <div className="App">
            {currentExercice === '' ? (
                <Fragment>
                    <div className="menusContainer">
                        <div className="menuGeneral">
                            <h2>Exercices de lecture</h2>
                            <Button
                                variant="contained"
                                onClick={() => {
                                    setCurrentExercice('Defilement')
                                }}
                            >
                                Défilement
                            </Button>
                            <Button
                                variant="contained"
                                onClick={() => {
                                    setCurrentExercice('MangeLettresMenu')
                                }}
                            >
                                Mange-Lettres
                            </Button>
                            <Button
                                variant="contained"
                                onClick={() => {
                                    setCurrentExercice('LectureFlash')
                                }}
                            >
                                Lecture Flash
                            </Button>
                            <Button
                                variant="contained"
                                onClick={() => {
                                    setCurrentExercice('Discrimination')
                                }}
                            >
                                Discrimination
                            </Button>
                            <Button
                                variant="contained"
                                onClick={() => {
                                    setCurrentExercice('Espacement')
                                }}
                            >
                                Espacement
                            </Button>
                            {showTestButton&&<Button
                                variant="contained"
                                onClick={() => {
                                    handleSubmit()
                                }}
                            >
                                Test Button
                            </Button>

                            }
                            
                        </div>
                    </div>
                </Fragment>
            ) : (
                <Fragment>
                    {currentExercice === 'Defilement' && <DefilementMenu back={back} />}
                    {currentExercice === 'MangeLettresMenu' && <MangeLettresMenu back={back} />}
                    {currentExercice === 'LectureFlash' && <LectureFlashMenu back={back} />}
                    {currentExercice === 'Discrimination' && <DiscriminationMenu back={back} />}
                    {currentExercice === 'Espacement' && <EspacementMenu back={back} />}
                </Fragment>
            )}
            <Footer />
        </div>
    )
}

export default App
