[![wakatime](https://wakatime.com/badge/user/dff37402-b315-4bad-899e-6a3f35924642/project/5fbccd40-03a0-4a04-9114-6b5116634fe2.svg)](https://wakatime.com/badge/user/dff37402-b315-4bad-899e-6a3f35924642/project/5fbccd40-03a0-4a04-9114-6b5116634fe2)
# CHANGE LOG
Tous les changement notables sont répértoriés dans ce fichier
## [0.9] - 2023-02-28
### Added
- Lecture Flash  : possibilité de choisir des mots de 1, 2 ou 3 lettres. Aussi le type dans ce cas ne peut pas être usuel ou irrégulier
## [0.8] - 2023-01-10
### Added
- Lecture Flash  : possibilité de choisir le nombre de lettres pour les mots
## [0.7] - 2022-12-15
### Added
- Espacement  : ajout nouvelle tâche lecture avec les mots du texte qui peuvent être espacées
## [0.6] - 2022-12-09
### Added
- Défilement  : ajout nouvelle tâche lecture avec le texte qui défile
### Changed
### Fixed
## [0.5] - 2022-12-08
### Added
- Discrimination  : Lorsque les distracteurs sont générés, si la limite d'itération est dépassée (= liste de mot différents atteinte), les lettres du mots cibles sont modifiées aléatoirement excepté la 1ère et la dernière 
 - Discrimination  : Ajout d'une consigne
 - Discrimination  : Score total à la fin de la session
  - Mange-lettres : Possibilité de choisir le texte dans le menu
 - Mange-lettres : Ajout de textes
### Changed
- Discrimination  : bouton pour lancer la session au lieu de cliquer sur la consigne
### Fixed
- Discrimination  : nombre de mots ne correspondait pas à la liste finale (n-1)
## [0.4] - 2022-12-07
### Added
 - Discrimination  : Ajout de la tâche discrimination
 
